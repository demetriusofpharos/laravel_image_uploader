# imageuploader

Image uploader for VictoryCTO evaluation.


1. Import `db/image_uploads.sql` to local database.
2. Copy `config/amazons3.yaml.example` to `config/amazons3.yaml`
    a. Fill out config info
3. For API, you can upload an image at `/upload/image` with the form param `image_upload`. This will return the S3 key for the full size image on success and an error on failure.
4. For the frontend:
    Navigate to `/images/all/` - you can specify `thumb` or `small` as the last URL param after `all` to change size. This is also in the basic navigation on the view. (Technically `full` also works.)
    Clicking on the images here will go to a page that loads the full size image.

Notes: 
    Image sizes can be configured in `config/imageuploader.yml`
