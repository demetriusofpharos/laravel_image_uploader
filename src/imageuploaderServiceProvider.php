<?php

namespace demetriusofpharos\imageuploader;

use \Illuminate\Support\ServiceProvider;

class imageuploaderServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        include __DIR__.'/routes.php';

        $this->loadViewsFrom(__DIR__ . '/Views', 'viewimage');

        // Publishing is only necessary when using the CLI.
        if ($this->app->runningInConsole()) {
            $this->bootForConsole();
        }
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make('demetriusofpharos\imageuploader\Controllers\UploadController');
        $this->app->make('demetriusofpharos\imageuploader\Models\ImageUploads');
        $this->app->make('demetriusofpharos\imageuploader\Helpers\AmazonS3Helper');
        $this->mergeConfigFrom(__DIR__ . '/../config/imageuploader.php', 'imageuploader');

        // Register the service the package provides.
        $this->app->singleton('imageuploader', function ($app) {
            return new imageuploader;
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['imageuploader'];
    }
    
    /**
     * Console-specific booting.
     *
     * @return void
     */
    protected function bootForConsole()
    {
        // Publishing the configuration file.
        $this->publishes([
            __DIR__.'/../config/imageuploader.php' => config_path('imageuploader.php'),
        ], 'imageuploader.config');
    }
}
