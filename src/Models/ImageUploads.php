<?php

namespace demetriusofpharos\imageuploader\Models;

use \Illuminate\Database\Eloquent\Model;
use \Illuminate\Support\Facades\DB;

class ImageUploads extends Model
{
	protected $table = 'image_uploads';

	public $standardAttributes = [
		'id',
		'uuid',
		'path',
		'created_at',
		'updated_at',
    ];

    /**
     * get image path from uuid and type
     *
     * @param string $uuid
     * @param string $type 
     *
     * return image
     */
    public function getFromUuid($uuid, $type="full") {
        $image = self::where(['uuid' => $uuid, 'type' => $type])->firstOrFail();

        return ($image) ? $image : FALSE ;
    }

    /**
     * get image path from uuid and type
     *
     * @param string $uuid
     * @param string $type 
     *
     * return image_uploads.path
     */
    public function getPath($uuid, $type="full") {
        $image = self::where(['uuid' => $uuid, 'type' => $type])->firstOrFail();

        return ($image) ? $image->path : FALSE ;
    }

    /**
     * get all images by type 
     *
     * @param string $type 
     *
     * return all images by path
     */
    public function getAll($type="full") {
        $images = self::where('type', $type)->get();

        return $images;
    }
}
