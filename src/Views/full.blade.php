<title>Full Image: {{ $uuid }}</title>

<style>
    html, body {
        width: 99%;
        height: 99%;
    }

    #imagecontainer {
        min-width: 500px;
        min-height: 500px;
        display: flex;
        text-align: center;
        border: 1px solid #CCCCCC;
        margin: 5px;
    }

    #imagecontainer IMG {
        margin: 10px;
    }
</style>

<center>
    <h2>IMAGES</h2>

    <div id="navigation">
        &ndash;&nbsp;<span><a href="/images/all/thumb">Thumbnails</a></span>&nbsp;
        &ndash;&nbsp;<span><a href="/images/all/small">Small</a></span>&nbsp;
    </div>

    <div id="imagecontainer">
        @php
            $url = \demetriusofpharos\imageuploader\Helpers\AmazonS3Helper::getUrl($img['path']); 
        @endphp
        <img src="{{ $url }}" />
    </div>
</center>

