<title>Image Thumbnails</title>

<style>
    html, body {
        width: 99%;
        height: 99%;
    }

    #main {
        display: flex;
        border: 1px solid black;
        padding: 5px;
    }

    #imagecontainer {
        display: flex;
        text-align: center;
        border: 1px solid #CCCCCC;
    }

    #imagecontainer IMG {
        margin: 10px;
    }
</style>

<center>
    <h2>IMAGES</h2>

    <div id="navigation">
        &ndash;&nbsp;<span><a href="/images/all/thumb">Thumbnails</a></span>&nbsp;
        &ndash;&nbsp;<span><a href="/images/all/small">Small</a></span>&nbsp;
    </div>

    <div id="main">
        @foreach ($images as $i)
            <div id="imagecontainer" style="width: {{ $attrs[$size]['x'] + 20 }}px; height: {{ $attrs[$size]['y'] + 20 }}px;">
                @php
                    $url = \demetriusofpharos\imageuploader\Helpers\AmazonS3Helper::getUrl($i['path']); 
                @endphp
                <a href="/images/full/{{ $i['uuid'] }}">
                    <img src="{{ $url }}" width="{{ $attrs[$size]['x'] }}" height="{{ $attrs[$size]['y'] }}" />
                </a>
            </div>
        @endforeach
    </div>
</center>
