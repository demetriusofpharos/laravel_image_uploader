<?php 

Route::get(
    'upload',
    function() {
        return response()->json("I can't you do that, Dave.", 400);
    }
);

Route::get(
    'images',
    function() {
        return response()->json("I can't you do that, Dave.", 400);
    }
);

Route::get(
    'images/all/{size?}',
    "demetriusofpharos\imageuploader\Controllers\ViewImageController@all"
);

Route::get(
    'images/full/{uuid?}',
    "demetriusofpharos\imageuploader\Controllers\ViewImageController@full"
);

Route::post(
    "upload/image",
    "demetriusofpharos\imageuploader\Controllers\UploadController@imageUpload"
);
