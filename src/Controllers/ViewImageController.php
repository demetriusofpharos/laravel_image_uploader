<?php

namespace demetriusofpharos\imageuploader\Controllers;

use \Symfony\Component\Yaml\Yaml;

use \App\Http\Controllers\Controller;

use \demetriusofpharos\imageuploader\Helpers\AmazonS3Helper;
use \demetriusofpharos\imageuploader\Models\ImageUploads;

class ViewImageController extends Controller
{
    private $amazonS3;
    private $yaml;

    public function __construct()
    {
        $this->amazonS3 = new AmazonS3Helper();
        $this->yaml     = Yaml::parse(file_get_contents(realpath(__DIR__ . '/../../config') . '/imageuploader.yaml'));
    }

    /**
     * View all uploaded images (thumbnail view)
     *
     * @param string $size (loaded from query)
     *
	 * @return view()
     */
    public function all($size = "thumb") {
        $i      = new ImageUploads();
        $images  = $i->getAll($size);
        $attrs  = $this->yaml["sizes"];

        return view("viewimage::all", ["images" => $images, "attrs" => $attrs, "size" => $size]);
    }

    /**
     * View full size image images (thumbnail view)
     *
     * @param string $size (loaded from query)
     *
	 * @return view
     */
    public function full($uuid = "") {
        $error = "";
        $img   = "";

        if (empty($uuid)) {
            $error = "No image provided.";

        } else {
            $i   = new ImageUploads();
            $img = $i->getFromUuid($uuid, "full");

            if (empty($img)) {
                $error = "No image found.";
            }
        }

        return view("viewimage::full", ["uuid" => $uuid, "error" => $error, "img" => $img]);
    }
}
