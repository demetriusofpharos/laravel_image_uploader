<?php

namespace demetriusofpharos\imageuploader\Controllers;

use \Illuminate\Http\Request;
use \Intervention\Image\Facades\Image;
use \Symfony\Component\Yaml\Yaml;

use \App\Http\Controllers\Controller;

use \demetriusofpharos\imageuploader\Helpers\AmazonS3Helper;
use \demetriusofpharos\imageuploader\Models\ImageUploads;

class UploadController extends Controller
{
    private $amazonS3;
    private $yaml;

    public function __construct()
    {
        $this->amazonS3 = new AmazonS3Helper();
        $this->yaml     = Yaml::parse(file_get_contents(realpath(__DIR__ . '/../../config') . '/imageuploader.yaml'));
    }
  
    /**
     * Upload a catalog file
     * accepts a file upload, processes it, sends it to S3
     *
     * @param Request $request
     *
	 * @return Response
     */
    public function imageUpload(Request $request) {
        if ($request->hasFile("image_upload")) {
            $upload = $request->file("image_upload");
            $name   = $upload->getClientOriginalName();
            $ext    = explode('.', $name);
            $ext    = array_pop($ext);
            $valid  = $this->isValidFile($upload);

            if (!is_array($valid)) {
                $files  = [];
                $uuid   = uniqid();

                list($width, $height) = getimagesize($upload->getPathName());
                $files['full'] = [
                    'x'     => $width,
                    'y'     => $height,
                    'path'  => $upload->getPathName()
                ];

                foreach ($this->yaml['sizes'] as $key => $values) {
                    if($values['x'] < $files['full']['x'] OR $values['y'] < $files['full']['y']) {
                        $newimg = $this->yaml['temp_path'] . $uuid . '_' . $key . '.' . $ext;
                        $img = Image::make($upload->getPathName());
                        $img->resize(
                            $values['x'], 
                            $values['y'], 
                            function ($constraint) {
                                $constraint->aspectRatio();
                                $constraint->upsize();
                            }
                        )->save($newimg);

                        $files[$key] = [
                            'x'     => $values['x'],
                            'y'     => $values['x'],
                            'path'  => $newimg,
                        ];
                    }
                }

                foreach ($files as $type => $file) {
                    // save to S3
                    $s3Upload = $this->amazonS3->config['bucket']['Prefix'] . $uuid . '_' . $type . '.' . $ext;
                    $this->amazonS3->uploadFile($file['path'], $s3Upload);
                    $dbimg          = new ImageUploads();
                    $dbimg->uuid    = $uuid;
                    $dbimg->path    = $s3Upload;
                    $dbimg->type    = $type;
                    $dbimg->save();

                    if ($type == "full") {
                        $s3Full = $s3Upload;
                    }

                    unlink($file['path']);
                }
                    
                $response = response()->json(["s3_key" => $s3Full], 201);

            } else {
                $response = response()->json(["error" => $valid], 400);
            }

        } else {
            $response = response()->json(["You must upload a file"], 404);
        }
        
        return $response;
    }


    /**
     * Validate file
     */
    private function isValidFile($file) {
        $errors = [];

        // <check_file_size>
        if (filesize($file->getPathName()) == 0) {
			$errors['file_size'] = 'file is empty';
		}
        // </check_file_size>

        // <check_mime_type>
        $mime = mime_content_type($file->getPathName());
        if (!in_array($mime, $this->yaml["mime_types"])) {
            $errors["mime_type"] = "The mime type $mime is not allowed.";
        }
        // </check_mime_type>

        return (count($errors) > 0) ? $errors : TRUE ;
    }
}
