<?php

namespace demetriusofpharos\imageuploader\Helpers;

use \Aws\Credentials\AssumeRoleCredentialProvider;
use \Aws\Credentials\CredentialProvider;
use \Aws\Credentials\Credentials;
use \Aws\S3\S3Client;
use \Aws\Sts\StsClient;
use \Symfony\Component\Yaml\Yaml;

class AmazonS3Helper
{
	public $client;
	public $config;

	protected $credentials;

    private $yaml;

	/**
	 * @inheritDoc
	 */
	public function __construct()
    {
        $this->yaml = Yaml::parse(file_get_contents(realpath(__DIR__ . '/../../config') . '/amazons3.yaml'));

		$this->__buildConfig();
		$this->__buildCredentials();
		$this->__buildClient();
	}

	/**
	 * download file
	 *
	 * @param string $file
	 *
	 * @return void
	 */
	public function downloadFile($file)
	{
		$bucket = $this->config['bucket'];

		$filename = array_reverse(explode('/', $file));

		if ($this->allowed($bucket)) {
			try {
				$result = $this->client->getObject([
					'Bucket' => $bucket['Bucket'],
					'Key'    => $file,
				]);

				return $result;
			} catch (Exception $e) {
				throw new \Exception("Cannot download file from Amazon: " . $e);
			}
		} else {
			return false;
		}
	}

	/**
	 * file info
	 *
	 * @param string $file
	 *
	 * @return void
	 */
	public function fileInfo($file)
	{
		$bucket = $this->config['bucket'];

		if ($this->allowed($bucket)) {
			try {
				$result = $this->client->getObject(['Bucket' => $bucket['Bucket'], 'Key' => $file]);
				return $result;
			} catch (Exception $e) {
				throw new \Exception("Cannot get file info from Amazon: " . $e);
			}
		} else {
			return false;
		}
	}

	/**
	 * static alias to self::filePath($file)
	 */
    public static function getUrl($file) {
        $aws = new self();
        return $aws->filePath($file);
    }

	/**
	 * download path
	 *  given a file, retrieve the download path
	 *
	 * @param string $file
	 *
	 * @return string
	 */
	public function filePath($file)
	{
		$bucket = $this->config['bucket'];

		if ($this->allowed($bucket)) {
			try {
				$name = array_reverse(explode('/', $file));
				$result = $this->client->getObjectUrl($bucket['Bucket'], $file, '5 minutes');
				return $result;
			} catch (Exception $e) {
				throw new \Exception("Cannot download file from Amazon: " . $e);
			}
		} else {
			return false;
		}
	}

	/**
	 * listFiles
	 *  run `ls` in a bucket
	 *
	 * @param string $bucket (bucket name)
	 * @param string $paginator ('use' | 'none')
	 *
	 * @return string (or false on fail)
	 */
	public function listFiles($bucket = '', $paginator = 'none')
	{
		if (empty($bucket)) {
			$bucket = $this->config['bucket'];
		}

		if ($this->allowed($bucket)) {
			try {
				$list = [];

				switch ($paginator) {
					case 'use':
					default:
						$unfiltered = $this->client->getPaginator('ListObjectsV2', $bucket);

						foreach ($unfiltered->search('Contents[].Key') as $key) {
							$list[] = [
								'Key' => $key,
								'Url' => $this->client->getObjectUrl($bucket['Bucket'], $key),
							];
						}
						break;

					case 'none':
						$unfiltered = $this->client->listObjectsV2($bucket);

						if (!empty($unfiltered['Contents'])) {
							$unfiltered = $unfiltered['Contents'];

							foreach ($unfiltered as $file) {
								$list[] = [
									'Date' => date('M d, Y H:i:s', strtotime($file['LastModified']->__toString())),
									'Key'  => $file['Key'],
									'Size' => $file['Size'],
									'Url'  => $this->client->getObjectUrl($bucket['Bucket'], $file['Key']),
								];
							}
						}
						break;
				}

				return $list;
			} catch (Exception $e) {
				throw new \Exception("Cannot list files: " . $e);
			}
		} else {
			return false;
		}
	}

	/**
	 * upload file
	 *
	 * @param string $file
	 * @param string $destination
	 *
	 * @return object (or false on fail)
	 */
	public function uploadFile($file, $destination)
	{
		$bucket = $this->config['bucket'];

		if ($this->allowed($bucket)) {
			$upload = [
				'ACL'        => 'bucket-owner-full-control',
				'Bucket'     => $bucket['Bucket'],
				'Key'        => $destination,
				'SourceFile' => $file,
			];

			try {
				$result = $this->client->putObject($upload);
				return $result;
			} catch (Exception $e) {
				throw new \Exception("Cannot upload file to Amazon: " . $e);
			}
		} else {
			return false;
		}
	}


	/**
	 * make sure we have permission to upload
	 *
	 * @param string $bucket
	 *
	 * @return boolean
	 */
	protected function allowed($bucket)
	{
		try {
			$access = $this->client->headBucket($bucket);

			if (!empty($access)) {
				return true;
			}
		} catch (Exception $e) {
			throw new \Exception("Cannot access specified bucket - check permissions: " . $e);
		}
		return false;
	}

	/**
	 * Provide a way to format the config var
	 *
	 * @param string $config
	 *
	 * @return object (or false on fail)
	 */
	protected function __buildConfig()
	{
		$config = [
			"bucket"        => [
				"Bucket"        => $this->yaml["amazonS3"]["bucket"]["Bucket"],
				"Delimeter"     => ",",
				"EncodingType"  => "url",
				"Prefix"        => $this->yaml["amazonS3"]["bucket"]["Prefix"],
			],
			"debug"         => false,
			"iam"           => $this->yaml["amazonS3"]["iam"],
			"key"           => $this->yaml["amazonS3"]["key"],
			"region"        => $this->yaml["amazonS3"]["region"],
			"role"          => $this->yaml["amazonS3"]["role"],
			"validate"      => false,
			"version"       => "latest",
			"secret"        => $this->yaml["amazonS3"]["secret"],
			"session_name"  => $this->yaml["amazonS3"]["session_name"],
		];
		$this->config = $config;
		return $this->config;
	}

	/**
	 * Provide a way to format the credentials var
	 *
	 * @return object (CredentialsProvider)
	 */
	protected function __buildCredentials()
	{
		$role = new AssumeRoleCredentialProvider([
			'client'             => new StsClient([
				'region'          => $this->config['region'],
				'version'         => $this->config['version'],
				'credentials'     => new Credentials($this->config['key'], $this->config['secret']),
				]),
			'assume_role_params' => [
				'RoleArn'         => $this->config['iam'],
				'RoleSessionName' => $this->config['session_name'],
			],
		]);

		$this->credentials = CredentialProvider::memoize($role);
		return $this->credentials;
	}

	/**
	 * Provide a way to format the client var
	 *
	 * @return object (S3Client version of config object)
	 */
	protected function __buildClient()
	{
		$this->client = new S3Client([
			'debug'         => $this->config['debug'],
			'region'        => $this->config['region'],
			'validate'      => $this->config['validate'],
			'version'       => $this->config['version'],
			'credentials'   => $this->credentials,
		]);
		return $this->client;
	}
}
